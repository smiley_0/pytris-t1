import pyglet
import types

from pyglet.window import key

from tetris import resources, keybindings
from tetris import solution
from tetris.tetrion import Tetrion

window = pyglet.window.Window(resizable=True, width=1000, height=600)
batch = pyglet.graphics.Batch()
fps = pyglet.clock.ClockDisplay()

p1tetrion = Tetrion(batch=batch)
p1tetrion.bindings = keybindings.MULTI_PLAYER1

p2tetrion = Tetrion(x_offset=500, batch=batch)
p2tetrion.bindings = keybindings.MULTI_PLAYER2

pp_logo = pyglet.sprite.Sprite(resources.python_powered, x=850, y=10, batch=batch)
samurai = pyglet.sprite.Sprite(resources.samurai, x=860, y=40, batch=batch)

garbage_lines = [0, 0, 1, 2, 4]  # Null, Single, Double, Triple, Tetris


@window.event
def on_draw():
    window.clear()
    batch.draw()
    fps.draw()


@window.event
def on_key_press(symbol, modifiers):
    if symbol == key.F5:
        p1tetrion.paused = not p1tetrion.paused
        p2tetrion.paused = p1tetrion.paused


def update(dt):
    p1tetrion.update(dt)
    p2tetrion.update(dt)


def mp_clear_lines(self):
    cleared = self._clear_lines_old()
    if cleared > 0:
        self._other.add_garbage(garbage_lines[cleared])


def monkey_patch_tetrion(target: Tetrion, other: Tetrion):
    target._other = other
    target._clear_lines_old = target.clear_lines
    target.clear_lines = types.MethodType(mp_clear_lines, target)


# Push tetrion handlers after defining on_key_press
window.push_handlers(p1tetrion.key_handler)
window.push_handlers(p2tetrion.key_handler)

# Monkey patch multiplayer glue code
monkey_patch_tetrion(p1tetrion, p2tetrion)
monkey_patch_tetrion(p2tetrion, p1tetrion)

if __name__ == '__main__':
    pyglet.clock.schedule_interval(update, 1 / 120.0)
    pyglet.app.run()
