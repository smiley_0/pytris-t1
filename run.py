import pyglet
from pyglet.window import key

from tetris import resources
from tetris import solution
from tetris.tetrion import Tetrion

window = pyglet.window.Window(resizable=True, height=600)
batch = pyglet.graphics.Batch()
fps = pyglet.clock.ClockDisplay()

tetrion = Tetrion(batch=batch)

pp_logo = pyglet.sprite.Sprite(resources.python_powered, x=500, y=10, batch=batch)
samurai = pyglet.sprite.Sprite(resources.samurai, x=510, y=40, batch=batch)


@window.event
def on_draw():
    window.clear()
    batch.draw()
    fps.draw()


@window.event
def on_key_press(symbol, modifiers):
    if symbol == key.F5:
        tetrion.paused = not tetrion.paused


# Push tetrion handlers after defining on_key_press
window.push_handlers(tetrion.key_handler)

if __name__ == '__main__':
    pyglet.clock.schedule_interval(tetrion.update, 1/120.0)
    pyglet.app.run()
