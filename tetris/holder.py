from tetris.pieces import Piece
from tetris.ui.common import Positionable, Scalable


class Holder(Scalable, Positionable):
    def __init__(self, x, y, scale=0.7) -> None:
        super().__init__()

        # Override default position and scale
        self._x = x
        self._y = y
        self._scale = scale

        self._piece = None

    def _update_position(self):
        self._piece.position = self.position
        self._piece.scale = self.scale

    def swap_piece(self, piece: Piece) -> Piece:
        ret = self._piece

        if ret is not None:
            ret.scale = 1

        self._piece = piece
        self._update_position()

        return ret
