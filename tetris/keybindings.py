from pyglet.window import key
from enum import Enum


class Key(Enum):
    LEFT = 0
    RIGHT = 1
    ROTATE_CW = 2
    ROTATE_CCW = 3
    SOFT_DROP = 4
    HARD_DROP = 5
    HOLD = 6


SINGLE_DEFAULT = [
    key.LEFT,
    key.RIGHT,
    key.UP,
    key.LCTRL,
    key.DOWN,
    key.SPACE,
    key.LSHIFT,
]

MULTI_PLAYER1 = [
    key.A,
    key.D,
    key.V,
    key.C,
    key.S,
    key.W,  # TODO Change?
    key.LSHIFT,
]

MULTI_PLAYER2 = [
    key.L,
    key.APOSTROPHE,
    key.UP,
    key.DOWN,
    key.SEMICOLON,
    key.P,  # TODO Change?
    key.RSHIFT,
]
