from abc import ABC, abstractmethod

import pyglet


class Positionable(ABC):
    def __init__(self) -> None:
        super().__init__()
        self._x = 0
        self._y = 0

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        self._x = x
        self._update_position()

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        self._y = y
        self._update_position()

    @property
    def position(self):
        return self._x, self._y

    @position.setter
    def position(self, pos):
        self._x, self._y = pos
        self._update_position()

    @abstractmethod
    def _update_position(self):
        pass


class Scalable(ABC):
    def __init__(self) -> None:
        super().__init__()
        self._scale = 1

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        self._scale = value
        self._update_position()

    @abstractmethod
    def _update_position(self):
        pass


class Primitive(Scalable, Positionable, ABC):
    draw_mode = pyglet.gl.GL_TRIANGLES

    def __init__(self, x, y, batch=None, group=None) -> None:
        super().__init__()
        self._x = x
        self._y = y

        self._batch = batch
        self._group = group
        self._vertex_list = None
        self._create_vertex_list()

    @abstractmethod
    def _create_vertex_list(self):
        pass

    def draw(self):
        if self._group is not None:
            self._group.set_state_recursive()

        self._vertex_list.draw(self.draw_mode)

        if self._group is not None:
            self._group.unset_state_recursive()

    def delete(self):
        self._vertex_list.delete()
        self._vertex_list = None
