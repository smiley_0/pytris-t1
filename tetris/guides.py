import pyglet

from tetris import colors
from tetris.common import BLOCK_SIZE
from tetris.ui.common import Primitive


class ColumnGuide(Primitive):
    def __init__(self, color, x, y, height, width=1, batch=None, group=None) -> None:
        self._color = color
        self.height = height
        self.width = width
        super().__init__(x, y, batch, group)

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        self._color = value
        self._update_color()

    def _create_vertex_list(self):
        indices = [0, 1, 2, 0, 2, 3]
        if self._batch is not None:
            self._vertex_list = self._batch.add_indexed(4, self.draw_mode, self._group, indices, 'v2i', 'c4B')
        else:
            self._vertex_list = pyglet.graphics.vertex_list_indexed(4, indices, 'v2i', 'c4B')

        self._update_position()
        self._update_color()

    def _update_position(self):
        vertices = (
            self.x, self.y,
            self.x + BLOCK_SIZE*self.width, self.y,
            self.x + BLOCK_SIZE*self.width, self.y + BLOCK_SIZE*self.height,
            self.x, self.y + BLOCK_SIZE*self.height,
        )
        self._vertex_list.vertices[:] = vertices

    def _update_color(self):
        c = self._color
        cd = colors.alpha(c, 50)
        cu = colors.alpha(c, 12)
        self._vertex_list.colors[:] = cd + cd + cu + cu

    def draw(self):
        super().draw()

    def update(self, piece, position):
        # TODO: Experimental, refactor on remove depending on playtests
        minx, maxx = len(piece.map), 0

        for r, row in enumerate(piece.map):
            for c, pc in enumerate(row):
                if pc[1] is None:
                    continue

                minx = min(minx, c)
                maxx = max(maxx, c)

        self.width = maxx - minx + 1
        self.x = position[0] + minx*BLOCK_SIZE
