from random import sample

from tetris import common
from tetris.pieces import pieces
from .common import BlockType


def generate_matrix(width=10, height=22):
    matrix = [[(BlockType.EMPTY, None) for _ in range(width+2)] for _ in range(height+1)]
    for c in range(width+2):
        matrix[0][c] = (BlockType.SENTINEL, None)

    for r in range(height+1):
        matrix[r][0] = (BlockType.SENTINEL, None)
        matrix[r][-1] = (BlockType.SENTINEL, None)
    return matrix


def generate_matrix_row(width=10):
    row = [(BlockType.EMPTY, None) for _ in range(width+2)]
    row[0] = (BlockType.SENTINEL, None)
    row[-1] = (BlockType.SENTINEL, None)
    return row


def lerp(a, b, t):
    return a + (b - a) * t


class BagGenerator(common.RandomGenerator):
    def __init__(self) -> None:
        super().__init__()
        self._keys = list(pieces.keys())
        self._sequence = []

    def _generate_sequence(self):
        self._sequence += sample(self._keys, len(self._keys))

    def next(self):
        if len(self._sequence) == 0:
            self._generate_sequence()

        return self._sequence.pop(0)

    def preview(self, count=3):
        if len(self._sequence) < count:
            self._generate_sequence()

        return self._sequence[0:count]
