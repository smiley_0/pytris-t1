from random import randint

import pyglet
from pyglet.window.key import KeyStateHandler

from tetris import gl_groups
from tetris.guides import ColumnGuide
from tetris.holder import Holder
from tetris.keybindings import Key
from tetris.common import BlockType, BLOCK_SIZE
from tetris.nextpiece import NextPiece
from tetris.util import BagGenerator
from .pieces import Block
from . import common, util, keybindings, colors


class Tetrion(common.AbstractGame):
    TETRION_COLOR = (80, 80, 80)
    DEFAULT_OFFSET_X = 15
    DEFAULT_OFFSET_Y = 60

    INIT_FALL_T = 0.3
    SOFT_DROP_T = 0.03
    LOCK_DELAY_T = 0.03

    def __init__(self, width=10, height=20, x_offset=None, y_offset=None, batch=None) -> None:
        super().__init__()
        self.width, self.height = width, height

        self._matrix = util.generate_matrix(self.width, self.height+2)
        self._generator = BagGenerator()
        self._garbage_buffer = []
        self.paused = False

        self.key_handler = KeyStateHandler()
        self.bindings = keybindings.SINGLE_DEFAULT

        self._batch = batch

        # Gravity
        self._fall_time = 0.0
        self._current_fall_t = self.INIT_FALL_T
        self._lock_delay = None

        # DAS
        self._das_key = None
        self._das_repeat = 0.0
        self._das_rotate_prev = False

        # Soft/hard drop
        self._soft_drop = False
        self._prev_hard_drop = False
        self._hard_drop = False

        # Scoring
        self._lines = 0
        self._score = 0

        # GL Groups
        self.fg_group = gl_groups.ForegroundGroup(1)
        self.effects_group = gl_groups.TransparentBgBlendGroup(0)

        # UI Offsets
        self.offset_x = x_offset if x_offset is not None else self.DEFAULT_OFFSET_X
        self.offset_y = y_offset if y_offset is not None else self.DEFAULT_OFFSET_Y

        # Hold
        self._can_hold = True
        self._prev_hold = False
        self._holder = Holder(self.offset_x + 270, self.offset_y + 400)

        # UI init
        self._score_label = pyglet.text.Label('Score: 0', x=self.offset_x+270, y=self.offset_y+100, batch=batch)
        self._lines_label = pyglet.text.Label('Lines: 0', x=self.offset_x+270, y=self.offset_y+80, batch=batch)
        self._preview = NextPiece(self._generator, self.offset_x+270, self.offset_y+150,
                                  batch=batch, group=self.fg_group)

        # Column guide
        self._guide = ColumnGuide(colors.scheme[colors.Color.CYAN], self.offset_x, self.offset_y, self.height,
                                  self.width, batch=batch, group=self.effects_group)
        self._target_color = colors.scheme[colors.Color.CYAN]

        self._calc_vertices()

    @property
    def matrix(self):
        return self._matrix

    @property
    def matrix_dimensions(self):
        return self.width, self.height

    def _calc_vertices(self):
        # Pre-calculate matrix and tetrion vertices
        matrix_lines = []
        for x in range(0, self.width + 1):
            matrix_lines += [
                x * common.BLOCK_SIZE, 0,
                x * common.BLOCK_SIZE, self.height * common.BLOCK_SIZE,
            ]
        for y in range(0, self.height + 1):
            matrix_lines += [
                0, y * common.BLOCK_SIZE,
                self.width * common.BLOCK_SIZE, y * common.BLOCK_SIZE,
            ]
        for i, v in enumerate(matrix_lines):
            matrix_lines[i] += self.offset_x if i % 2 == 0 else self.offset_y
        vert_count = len(matrix_lines) // 2
        if self._batch is not None:
            self._vertex_list = self._batch.add(vert_count, pyglet.gl.GL_LINES, self.effects_group,
                                                ('v2i', matrix_lines),
                                                ('c3B', self.TETRION_COLOR * vert_count))
        else:
            self._vertex_list = pyglet.graphics.vertex_list(vert_count, ('v2i', matrix_lines),
                                                            ('c3B', self.TETRION_COLOR * vert_count))

    def draw(self):
        self.effects_group.set_state_recursive()
        self._vertex_list.draw(pyglet.gl.GL_LINES)
        self.effects_group.unset_state_recursive()

    def add_offset(self, px, py):
        return px+self.offset_x, py+self.offset_y

    def try_das(self, dt, key):
        if self._das_key != key:
            self._das_key = key
            self._das_repeat = 0.0
            return True

        self._das_repeat += dt
        if self._das_repeat > 0.07:
            self._das_repeat = 0.0
            return True

        return False

    def is_key(self, k):
        return self.key_handler[self.bindings[k.value]]

    def update(self, dt):
        if self.paused:
            return

        pp = self._piece_pos[:]

        self.handle_speed_mods()
        self.handle_hold()

        if self._piece is None:
            self.spawn_piece()
            self.generate_garbage()
            self._fall_time = 0.0
        elif not self._hard_drop:
            effective_t = self._current_fall_t
            if self._soft_drop:
                effective_t = self.SOFT_DROP_T

            self._fall_time += dt
            if self._fall_time > effective_t:
                self._fall_time = 0.0
                self._piece_pos[1] -= 1
        else:
            self.do_hard_drop()
            self.lock_piece()
            self.clear_lines()
            return

        # Detect collisions caused by gravity and start lockdown timer
        if self.detect_matrix_collision():
            # Collision detected, return piece back
            self._piece_pos = pp[:]

            if self._lock_delay is None:
                # Activate countdown if first frame of gravity caused collision
                self._lock_delay = 0.0
            elif self._lock_delay > self.LOCK_DELAY_T:
                # Lock the piece on timeout
                self.lock_piece()
                self.clear_lines()
                self._can_hold = True
                self._lock_delay = None
                return
            else:
                # Just add dt to timer
                self._lock_delay += dt
        elif self._fall_time == 0.0:
            # Disarm countdown if piece moved down due to gravity
            self._lock_delay = None

        self.handle_movement(dt)

        # Detect other collisions
        if self.detect_matrix_collision():
            self._piece_pos = pp[:]

        self.handle_rotation()

        # Update piece position
        self._piece.position = self.add_offset(*self.piece_position_px)

        # EXPERIMENTAL: Update column guide
        self._guide.update(self.piece, self.add_offset(*self.piece_position_px))

        self.clear_lines()

        self._guide.color = colors.lerp(self._guide.color, self._target_color, dt*5.0)

    def spawn_piece(self):
        self._set_piece(self._preview.pop_piece())

    def _post_piece_actions(self):
        super()._post_piece_actions()

        if self.piece is None:
            return

        piece_side = len(self._piece.map)
        self._piece_pos = [
            (self.width // 2) - int(piece_side / 2 + 0.5),
            self.height - piece_side + 1
        ]
        self._target_color = colors.scheme[self._piece.color]

    def do_hard_drop(self):
        while not self.detect_matrix_collision():
            self._piece_pos[1] -= 1
        self._piece_pos[1] += 1  # Compensate

        # Update piece position
        self._piece.position = self.add_offset(*self.piece_position_px)
        self._hard_drop = False
        self._can_hold = True

    def handle_speed_mods(self):
        self._soft_drop = self.is_key(Key.SOFT_DROP)

        if self.is_key(Key.HARD_DROP):
            if not self._prev_hard_drop:
                self._prev_hard_drop = True
                self._hard_drop = True
        else:
            self._prev_hard_drop = False

    def handle_hold(self):
        if not self._can_hold:
            return

        if self.is_key(Key.HOLD):
            if not self._prev_hold:
                self._prev_hold = True
            else:
                return
        else:
            self._prev_hold = False
            return

        # Swap pieces
        self._set_piece(self._holder.swap_piece(self.piece))
        self._can_hold = False

    def handle_movement(self, dt):
        if self.is_key(Key.LEFT):
            if self.try_das(dt, Key.LEFT):
                self._piece_pos[0] -= 1  # yuck
        elif self.is_key(Key.RIGHT):
            if self.try_das(dt, Key.RIGHT):
                self._piece_pos[0] += 1  # yuck again
        else:
            self._das_repeat = 0.0
            self._das_key = None

    def handle_rotation(self):
        rotation = None

        if self.is_key(Key.ROTATE_CW):
            if not self._das_rotate_prev:
                self._das_rotate_prev = True
                rotation = True
        elif self.is_key(Key.ROTATE_CCW):
            if not self._das_rotate_prev:
                self._das_rotate_prev = True
                rotation = False
        else:
            self._das_rotate_prev = False

        if rotation is None:
            return

        self._piece.rotate(rotation)

        # TODO SRS
        if self.detect_matrix_collision():
            self._piece.rotate(not rotation)

        return rotation

    def clear_lines(self):
        lines = 0
        for r in range(self.height+2, 0, -1):
            lines += self.clear_line(r)

        self.update_block_positions()

        if lines > 0:
            self._lines += lines
            self._lines_label.text = 'Lines: %d' % self._lines

        return lines

    def update_block_positions(self):
        for r, row in enumerate(self.matrix):
            for c, block in enumerate(row):
                block = block[1]
                if block is None:
                    continue

                block.position = self.add_offset(
                    (c - 1) * BLOCK_SIZE,
                    (r - 1) * BLOCK_SIZE
                )

    def clear_line(self, row):
        # Is full line?
        for block_type, block in self.matrix[row][1:-1]:
            if block_type == BlockType.EMPTY:
                return 0

        # Remove block objects for line
        for block_type, block in self.matrix[row][1:-1]:
            block.delete()

        # Delete line
        self.matrix.pop(row)

        # Add clear line from top
        self.matrix.append(util.generate_matrix_row(self.width))

        return 1

    def add_garbage(self, rows=1):
        if rows <= 0:
            return
        self._garbage_buffer.append(rows)

    def generate_garbage(self):
        garbage_lines = []
        for line_rows in self._garbage_buffer:
            gap = randint(1, self.width)  # 1 to compensate sentinels

            for _ in range(line_rows):
                new_line = util.generate_matrix_row(self.width)
                for i, el in enumerate(new_line):
                    if el[0] == BlockType.SENTINEL or i == gap:
                        continue

                    # No need to calculate positions here
                    new_line[i] = (BlockType.NORMAL, Block(colors.Color.UNDEF, batch=self._batch, group=self.fg_group))

                garbage_lines.append(new_line)

        self._matrix = [self._matrix[0]] + garbage_lines + self._matrix[1:len(self._matrix) - len(garbage_lines)]

        self.update_block_positions()
        self._garbage_buffer.clear()
