import pyglet
from pyglet import gl


class TransparentBgBlendGroup(pyglet.graphics.OrderedGroup):
    def set_state(self):
        super().set_state()
        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

    def unset_state(self):
        gl.glDisable(gl.GL_BLEND)
        super().unset_state()


class ForegroundGroup(pyglet.graphics.OrderedGroup):
    def set_state(self):
        super().set_state()
        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_ONE, gl.GL_ZERO)

    def unset_state(self):
        gl.glDisable(gl.GL_BLEND)
        super().unset_state()
