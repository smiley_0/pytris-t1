from abc import ABC, abstractmethod
from enum import Enum

BLOCK_SIZE = 24


class BlockType(Enum):
    SENTINEL = -1
    EMPTY = 0
    NORMAL = 1
    ORB = 2


class AbstractGame(ABC):
    def __init__(self) -> None:
        super().__init__()
        self._piece = None
        self._piece_pos = [0, 0]

        self._hold = None

    @property
    @abstractmethod
    def matrix(self):
        pass

    @property
    @abstractmethod
    def matrix_dimensions(self):
        pass

    @abstractmethod
    def update(self, dt):
        pass

    @property
    def piece(self):
        return self._piece

    def _set_piece(self, value):
        self._piece = value
        self._post_piece_actions()

    def _post_piece_actions(self):
        pass

    @property
    def piece_position(self):
        return self._piece_pos

    @property
    def piece_position_px(self):
        x, y = self.piece_position
        return x*BLOCK_SIZE, y*BLOCK_SIZE

    @property
    def hold(self):
        return self._hold

    def detect_matrix_collision(self, piece=None, piece_pos=None):
        if piece is None:
            piece = self.piece
        if piece_pos is None:
            piece_pos = self.piece_position

        px, py = piece_pos

        for r, row in enumerate(reversed(piece.map)):
            for c, block in enumerate(row):
                mx = px + c + 1
                my = py + r + 1
                try:
                    if self.matrix[my][mx][0] != BlockType.EMPTY and block[0] != BlockType.EMPTY:
                        return True
                except IndexError:
                    return True

        return False

    def lock_piece(self, piece=None, piece_pos=None, remove=True):
        if piece is None:
            piece = self.piece
        if piece_pos is None:
            piece_pos = self.piece_position

        px, py = piece_pos

        for r, row in enumerate(reversed(piece.map)):
            for c, block in enumerate(row):
                mx = px + c + 1
                my = py + r + 1
                if self.matrix[my][mx][0] == BlockType.EMPTY:
                    self.matrix[my][mx] = block

        self._piece = None
        self._piece_pos = [0, 0]


class RandomGenerator(ABC):
    @abstractmethod
    def next(self):
        pass

    @abstractmethod
    def preview(self, pieces=3):
        pass
