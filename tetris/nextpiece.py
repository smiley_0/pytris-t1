from tetris.common import RandomGenerator, BLOCK_SIZE
from tetris.pieces import Piece
from tetris.ui.common import Positionable, Scalable


class NextPiece(Scalable, Positionable):
    def __init__(self, generator: RandomGenerator, x, y, preview_size=3, scale=0.7, batch=None, group=None) -> None:
        super().__init__()

        # Override default position and scale
        self._x = x
        self._y = y
        self._scale = scale

        self._generator = generator
        self._preview_size = preview_size
        self._pieces = []

        self._batch = batch
        self._group = group

        self._fill_pieces()

    def _fill_pieces(self):
        while len(self._pieces) < self._preview_size:
            new_piece = self._generator.next()
            self._pieces.append(Piece(new_piece, batch=self._batch, group=self._group))

        self._update_position()

    def _update_position(self):
        spacing = 4 * BLOCK_SIZE * self.scale
        for i, p in enumerate(reversed(self._pieces)):
            p.scale = self.scale
            p.position = (
                self.x,
                self.y + i * spacing
            )

    def pop_piece(self) -> Piece:
        next_piece = self._pieces.pop(0)
        next_piece.scale = 1
        self._fill_pieces()
        return next_piece
