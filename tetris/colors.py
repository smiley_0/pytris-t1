from enum import Enum

from tetris import util


class Color(Enum):
    UNDEF = 0
    CYAN = 1
    YELLOW = 2
    PURPLE = 3
    GREEN = 4
    RED = 5
    BLUE = 6
    ORANGE = 7

    @staticmethod
    def from_block_letter(letter):
        return Color._mapping[letter]


Color._mapping = {
    'I': Color.CYAN,
    'O': Color.YELLOW,
    'T': Color.PURPLE,
    'S': Color.GREEN,
    'Z': Color.RED,
    'J': Color.BLUE,
    'L': Color.ORANGE,
}


scheme = {
    Color.UNDEF: (160, 160, 160),
    Color.CYAN: (0, 255, 255),
    Color.YELLOW: (255, 255, 0),
    Color.PURPLE: (255, 0, 255),
    Color.GREEN: (0, 255, 0),
    Color.RED: (255, 0, 0),
    Color.BLUE: (51, 51, 255),
    Color.ORANGE: (255, 128, 0),
}


def shade(color, shade_factor=0.5):
    r, g, b = color
    n_r = int(r * (1 - shade_factor))
    n_g = int(g * (1 - shade_factor))
    n_b = int(b * (1 - shade_factor))

    return n_r, n_g, n_b


def alpha(color, a):
    r, g, b = color
    return r, g, b, a


def lerp(c1, c2, t):
    r1, g1, b1 = c1
    r2, g2, b2 = c2
    n_r = util.lerp(r1, r2, t)
    n_g = util.lerp(g1, g2, t)
    n_b = util.lerp(b1, b2, t)

    return int(n_r), int(n_g), int(n_b)
