import pyglet

from tetris.ui.common import Primitive, Positionable, Scalable
from .common import BlockType, BLOCK_SIZE
from . import colors

pieces = {
    'I': [
        (0, 0, 0, 0),
        (1, 1, 1, 1),
        (0, 0, 0, 0),
        (0, 0, 0, 0),
    ],

    'J': [
        (1, 0, 0),
        (1, 1, 1),
        (0, 0, 0),
    ],

    'L': [
        (0, 0, 1),
        (1, 1, 1),
        (0, 0, 0),
    ],

    'O': [
        (1, 1),
        (1, 1),
    ],

    'S': [
        (0, 1, 1),
        (1, 1, 0),
        (0, 0, 0),
    ],

    'Z': [
        (1, 1, 0),
        (0, 1, 1),
        (0, 0, 0),
    ],

    'T': [
        (0, 1, 0),
        (1, 1, 1),
        (0, 0, 0),
    ],
}

# Replace ints with enums
for k, v in pieces.items():
    for r, row in enumerate(v):
        pieces[k][r] = list(BlockType.NORMAL if val == 1 else BlockType.EMPTY for val in row)


class Block(Primitive):
    draw_mode = pyglet.gl.GL_QUADS

    def __init__(self, color, x=0, y=0, batch=None, group=None) -> None:
        self._color = color
        super().__init__(x, y, batch, group)

    def _create_vertex_list(self):
        if self._batch is not None:
            self._vertex_list = self._batch.add(4, self.draw_mode, self._group, 'v2f', 'c3B')
        else:
            self._vertex_list = pyglet.graphics.vertex_list(4, 'v2f', 'c3B')

        self._update_position()
        self._update_color()

    def _update_position(self):
        bs = BLOCK_SIZE * self.scale
        vertices = (
            self.x, self.y,
            self.x + bs, self.y,
            self.x + bs, self.y + bs,
            self.x, self.y + bs,
        )
        self._vertex_list.vertices[:] = vertices

    def _update_color(self):
        c = colors.scheme[self._color]
        cd = colors.shade(c)
        cx = colors.shade(c, 0.3)
        self._vertex_list.colors[:] = cd + cd + cx + c


class Piece(Scalable, Positionable):
    def __init__(self, tetromino_type, batch=None, group=None) -> None:
        super().__init__()
        self._x = 0
        self._y = 0
        self._color = colors.Color.from_block_letter(tetromino_type)

        self._map = [list(row) for row in pieces[tetromino_type]]

        for r, row in enumerate(self._map):
            for c, blk in enumerate(row):
                # Re-pack
                self._map[r][c] = (blk, None)

                if blk != BlockType.NORMAL:
                    continue

                block = Block(self.color, batch=batch, group=group)
                self._map[r][c] = (blk, block)

        self._update_position()

    @property
    def map(self):
        return self._map

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        self._color = value
        self._update_color()

    def _update_position(self):
        max_r = len(self._map) - 1

        for r, row in enumerate(self._map):
            bs = BLOCK_SIZE * self.scale
            for c, pc in enumerate(row):
                _, piece = pc
                if piece is None:
                    continue

                piece.position = (self._x + c*bs, self._y + (max_r-r)*bs)
                piece.scale = self.scale

    def _update_color(self):
        for r in self.map:
            for _, piece in r:
                if piece is not None:
                    piece.color = self.color

    def rotate(self, right=True):
        if right:
            self._map = list(zip(*reversed(self._map)))
        else:
            self._map = list(zip(*self._map))[::-1]
        self._update_position()
