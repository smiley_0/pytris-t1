import pyglet


def center_image(image):
    """Sets an image's anchor point to its center"""
    image.anchor_x = image.width / 2
    image.anchor_y = image.height / 2


# Tell pyglet where to find the resources
pyglet.resource.path = ['./res']
pyglet.resource.reindex()

python_powered = pyglet.resource.image('py-d.png')
samurai = pyglet.resource.image('samurai.png')
